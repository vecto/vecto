
## VECTO v4.3.3 Official Release (04-03-2025)


### Bug Fixes

- Check if XML element is signed (#950) (vecto/vecto!314)

- NgTankSystem optional for HEV lorries MRF XSD (vecto/vecto!315)

- fix: monitoring report for dual fuel vehicles (vecto/vecto!318)

- fix: secure XML loading against external entity injection (vecto/vecto!319)

- fix: correct interim supercap reader type (vecto/vecto!320)

- fix: parameter IDs for XSDs v2.3 and v2.6 (vecto/vecto!322)

### Documentation

- Update XSD parameter IDs documentation (vecto/vecto!327)

