## PTO power demand during drive (.vptor)


**Example:**

~~~
t  , PTO_Power
0  , 20
10 , 20
12 , 25
15 , 40
25 , 20
28 , 5
30 , 0
~~~
