﻿namespace VECTO3GUI2020.Helper
{
	public interface IWindowHelper
	{
		void ShowWindow(object viewModel);
	}
}