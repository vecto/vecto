﻿namespace TUGraz.VectoCommon.Models
{
	public enum OvcHevMode
	{
		NotApplicable,
		ChargeSustaining,
		ChargeDepleting,
	}
}