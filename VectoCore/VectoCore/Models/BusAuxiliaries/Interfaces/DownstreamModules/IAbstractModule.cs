﻿namespace TUGraz.VectoCore.Models.BusAuxiliaries.Interfaces.DownstreamModules {
	public interface IAbstractModule
	{
		void ResetCalculations();
	}
}
