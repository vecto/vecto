﻿using TUGraz.VectoCore.OutputData.XML.DeclarationReports.Common;

namespace TUGraz.VectoCore.OutputData.XML.DeclarationReports.CustomerInformationFile.CustomerInformationFile_0_9.
	ResultWriter
{

	public interface IMRFResultsWriterFactory : ICommonResultsWriterFactory { }

}