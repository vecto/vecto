﻿using System.ComponentModel;

namespace VECTO3GUI.ViewModel.Interfaces {
	public interface IMainView
	{
		void Closing(object sender, CancelEventArgs e);
	}
}